When disaster strikes you must act quickly. 1-800 Water Damage of Southeast Michigan is on call 24/7 and ready to act encase of emergency. Our services go beyond water damage restoration to include fire/smoke restoration, mold remediation, and much more. With the backing of a national corporation, our locally owned & operated franchise can provide the quickest and best service to those living in Southeast Michigan and the surrounding areas. From small homes to large businesses, we provide services you can rely on!

Website: https://www.1800waterdamage.com/southeast-michigan/
